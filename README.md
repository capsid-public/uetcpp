# UETcpp

## Requirement
  - g++

## Compilation

```sh
g++ -fopenmp -std=c++17 src/main.cpp -o uet -I./include
```

## Usage
```sh
./uet [OPTION...]

  -p, --path arg       Data path (csv file, required)
  -s, --sep arg        Separator (default: '\t')
  -c, --ctypes arg     Coltypes (default: 0,)
  -n, --nmin arg       Nmin (default: 0.33)
  -t, --ntrees arg     Number of trees (default: 500)
  -m, --massbased arg  Mass-based dissimilarity (default: 0)
  -o, --optimize arg   Find optimal parameters (default: 0)
  -h, --help           Print help info

```

## Reference
Dalleau, K., Couceiro, M. & Smail-Tabbone, M. Unsupervised extra trees: a stochastic approach to compute similarities in heterogeneous data. *Int J Data Sci Anal* **9**, 447–459 (2020). https://doi.org/10.1007/s41060-020-00214-4